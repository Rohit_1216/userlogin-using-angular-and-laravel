<!DOCTYPE html>
<html>
  <head>
    <title>Welcome Email</title>
  </head>
  <body>
    <h2>Welcome to the site {{ $data['userName']}}</h2>
    <br/>
    Your registered email-id is {{ $data['email']}} , Please click on the below link to verify your email account
    <br/>
    <a href="{{url('verify',$data->verifyUser->token)}}">Verify Email</a>
    {{-- <a href="{{ url('verify'),$data->token }}"></a> --}}
  </body>
</html>
