<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;


    public function verifyUser()
    {
      return $this->hasOne('App\EmailVerification');

        // return $this->token === null;
    }



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamp=true;

    protected $fillable = [
        'firstName','lastName','userName','dateOfBirth','gender','token', 'email', 'password','role','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    public function getJWTCustomClaims()
    {
        return [];
    }

    // public function setPasswordAttributes($data)
    // {
    //   $this->attributes['password']= md5($data);
    // }
}
