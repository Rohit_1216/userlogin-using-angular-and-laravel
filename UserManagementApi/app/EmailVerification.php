<?php

namespace App;

use App\User;

use Illuminate\Database\Eloquent\Model;

class EmailVerification extends Model
{
    public $timestamp=true;
    protected $fillable = [
        'email','token'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'email');
    }


    public function verified($token)
    {
        return $this->token === null;
    }
}
