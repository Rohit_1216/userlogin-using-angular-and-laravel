<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
// use Illuminate\Http\Resources\Json\ResourceCollection;

class userCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        [
            'firstName'=>$this->firstName,
            'lastName'=>$this->lastName,
            'email'=>$this->email,
            'userName'=>$this->userName,
            'dateOfBirth'=>$this->dateOfBirth,
            'gender'=>$this->gender

        ];
    }
}
