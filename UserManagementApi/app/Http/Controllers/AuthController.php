<?php

namespace App\Http\Controllers;

use App\User;
use App\Mail\VerifyMail;

use App\EmailVerification;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Requests\SignupRequest;
use Illuminate\Support\Facades\Mail;
use App\Http\Resources\userCollection;
use Illuminate\Support\Facades\Response;


class AuthController extends Controller
{
    public function index()
    {
        // return  userCollection::collection(User::paginate(5));
        return  User::paginate(5);
    }



    public function login(Request $request)
    {
        $credentials = $request->only('email','password');
        if ($token = $this->guard()->attempt($credentials))
        {
            return $this->respondWithToken($token);
        }
        return \response()->json(['error' => 'Email or Password Does\'nt Exist']);
    }


    public function signup(SignupRequest $request)
    {
        $data = User::create([
            'firstName' => $request->get('firstName'),
            'lastName' => $request->get('lastName'),
            'password' => md5($request->get('password')),
            'userName' => $request->get('userName'),
            'token' => sha1(time()),
            'email' => $request->get('email'),
            'dateOfBirth' => $request->get('dateOfBirth'),
            'gender' => $request->get('gender'),
            'role' => ('user')
        ]);


            Mail::send('view', ['data' => $data], function ($m) use ($data) {
            $m->from('rg.gautam12@gmail.com', 'User Management System');

            $m->to([$data->email],[$data->firstName])->subject('EMAIL VERIFICATION');
        });
            return ($data);

    }


    public function verifyUser($token)
    {
        $verifyUser = User::where('token', $token)->first();
        if(isset($verifyUser) )
        {
            $testdata = $verifyUser->testdata;
        if(!$testdata->status)
        {
            $verifyUser->testdata->status = 1;
            $verifyUser->testdata->save();
            $status = "Your e-mail is verified. You can now login.";
        }
        else
        {
          $status = "Your e-mail is already verified. You can now login.";
        }
      }
      return redirect('http://localhost:4200/login');

    }




    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function me()
    {
        return \response()->json($this->guard()->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */


     public function logout()
    {
        $this->guard()->logout();

        return \response()->json(['message' => 'Successfully logged out']);
    }


    /**
    * Refresh a token.

     * @return \Illuminate\Http\JsonResponse
     */


     public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }


    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */


     protected function respondWithToken($token)
    {
        return \response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 1000
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */


     public function guard()
    {
        return Auth::guard();
    }

    protected function create(array $data)
    {


    }
}
